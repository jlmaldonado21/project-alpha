from django import forms
from tasks.models import Task


class CreateTask(forms.ModelForm):
    class Meta:
        model = Task
        exclude = ["is_completed", "owner"]
