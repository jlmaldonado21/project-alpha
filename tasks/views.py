from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import CreateTask
from tasks.models import Task

# from django.contrib.auth.models import User


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTask(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateTask()
    context = {
        "form": form,
    }

    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    assigned_tasks = Task.objects.filter(assignee=request.user)
    context = {
        "show_my_tasks": assigned_tasks,
    }
    return render(request, "tasks/tasks.html", context)
